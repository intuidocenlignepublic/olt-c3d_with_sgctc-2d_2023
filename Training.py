#%% md

# Training model

#%%

import os
import random
import sys
from datetime import datetime
from math import ceil
from shutil import copytree
from typing import Callable

import wandb
from wandb.keras import WandbCallback
import tensorflow as tf
import numpy as np

from LossesAndMetrics.LossesAndMetrics_CTC_CustomRec import lossCTCSimpleCustomRec_prior, lossCTCSimpleCustomRec, \
    lossCTCLiuNormal, lossCTC_Simple_TF
from LossesAndMetrics.LossesAndMetrics_basic import lossSmoothingPredictionCE, lossPerFrame, \
    TAR_Argmax_allValues_onlyClasses, RejectRate_Argmax_onlyClasses, RejectRate_Argmax_allValues, TAR_FAR_Argmax_Diff, \
    FAR_Argmax_allValues_onlyClasses, TAR_3FAR_Argmax_Diff
from Model.ModelEarly2D import OLTC3D_simpleStream
import tensorflow_addons as tfa

# pathDB = "C:\\workspace2\\Datasets\\2D\\MTG\\MTGSetB\\"

dataset = sys.argv[1]
weightPrior = float(sys.argv[2])

pathDB = "/srv/tempdd/wmocaer/data/2D/"+dataset+"/"
# pathDB = "C:\\workspace2\\Datasets\\2D\\"+dataset+"\\"
separator = "/"
pathPreprocessedData = pathDB+"PreprocessedUntrimmed/Train/"
pathPreprocessedDataValid = pathDB+"PreprocessedUntrimmed/Valid/"
pathCount = pathDB+"PreprocessedUntrimmed/Traincount"
pathCountValid = pathDB+"PreprocessedUntrimmed/Validcount"
pathLog =pathDB+"Log/"
physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
# tf.config.run_functions_eagerly(True)
#config = tf.config.experimental.set_memory_growth(physical_devices[0], True)

f = open(dataset+"/config.txt",'r')
configParams = f.readlines()
f.close()
configParams = eval("\n".join(configParams))

multiplierCoord = configParams["multiplierCoord"]
thresholdCuDi = configParams["thresholdCuDi"]# value in pixel
dimensionsImage,canal = np.array(configParams["dimensionsOutputImage"]),2
nbClass=configParams["nbClass"]

fCount = open(pathCount, "r")
countTrain = int(fCount.readlines()[0])
fCount.close()

fCount = open(pathCountValid, "r")
countValid = int(fCount.readlines()[0])
fCount.close()

date=datetime.now().strftime("%Y%m%d-%H%M%S")
pathWeight = pathLog+date+separator+"Weights"
#%% md

### Define hyper-parameters

#%%
boxSize = np.concatenate((dimensionsImage, [canal]))
nbTrain = countTrain
nbValid = countValid
dilatationRates = [1, 2, 4, 8, 16]
config = {
    "batchSize": 5,
    "doSSG": True,
    # weight of the CTC loss, weight of CE per frame is (1-weightCTCLoss). if 0, only the per-frame loss is used
    "weightCTCLoss": 1.0,
    "weightPrior": weightPrior,
    "weightSmoothing": 10.0,
    # weight of the background class (no actions), used for per-frame loss only
    "weightOfBG": 1.0,
    "dropoutValueConv": 0.1,
    "denseSize": 100,
    "denseDropout": 0.2,
    "nbFeatureMap": 30,
    "dilatationRates": dilatationRates,
    "numberOfBlocks": 2,
    "doGlobalAveragePooling": True,
    "doMaxPoolSpatial": True,
    "kernelSize": 3,
    # size of the kernel on the time dimension. Only tested with 2.
    "kernelSizeTemporalMain": 2,
    "poolSize": 3,
    "poolStride": 3,
    "nbDenseLayer": 1,
    "train_size": countTrain,
    "val_size": countValid,
    "boxSize": boxSize,
    "pathPreprocessedData": pathPreprocessedData,
    # use the callback to reduce the learning rate when the loss is not decreasing
    "useReduceLRCallBack": True,
    "useSegmentationGuidedCTC": True,
    # in the case where segmentation guided CTC is not used, and prior is not used
    # the parameter "useCTCLiu" is used to choose between the implementation of
    # the CTC loss of Liu et al. (used to implement the Segmentation-GuidedCTC)
    # or the CTC loss implemented directly in Tensorflow.
    # This avoid to have differences in the results because of the implmentation choice of CTC.
    # However the Tensorflow version is much faster.
    "useCTCLiu": False,
    # label prior will be used in the CTC loss computation (whatever version, either SG CTC or Liu CTC)
    # It can not be used with the Tensorflow implementation of CTC
    "usePrior": True,
    # use the validation set for training (useful for final tuning after that the HP has been fixed)
    # in this case the training will end after "epochMax" epochs
    "useValidationInTrain": False,
    # maximum number of epochs for training, whatever happens
    "epochMax": 600,
    # the path to the folder where the weights will be saved
    "localPathWeight": pathWeight,
    # if resumeRunName is not None, the training will be resumed from the weights of the run with this name
    # it will first try to download weights from wandb, then it will try to load from the local path "localPathWeight"
    "resumeRunName": None,
    # Note that all these parameters will be charged from the previous run if resumeRunName!= None
}
if config["useValidationInTrain"]:
    config["train_size"] = countTrain + countValid
    config["val_size"] = 0

configAll = config


#%% md

### Initialize wandb

#%%

tags = [dataset,"TempRejectPerSeq"]
wandBRun = wandb.init(project="precoce2d-CustomGraph",entity="intuidoc-gesture-reco",save_code=True,reinit=True,tags=tags,config=config)
config = wandb.config # will set new hyperparameters when sweep used
wandb.summary["dataset"]=dataset
wandb.summary["canaux"]=canal
wandb.summary["nbClass"]=nbClass
wandb.summary["receptiveField"]=sum(config.dilatationRates)
wandb.summary["nbConvLayers"]=len(config.dilatationRates)

print("WANDB run name = "+ wandBRun.name)
wandBRunDir = wandBRun.dir

#%% md

### Define the model

#%%
model = OLTC3D_simpleStream(nbClass=nbClass, boxSize=config.boxSize,
                           dropoutVal=config.dropoutValueConv, denseNeurones=config.denseSize,
                           denseDropout=config.denseDropout, nbFeatureMap=config.nbFeatureMap,
                           dilatationsRates=config.dilatationRates, numberOfBlock=config.numberOfBlocks,
                           doMaxPoolSpatial=config.doMaxPoolSpatial,
                           poolSize=(1, config.poolSize, config.poolSize),
                           poolStrides=(1, config.poolStride, config.poolStride), nbLayerDense=config.nbDenseLayer,
                           kernelSize=(config.kernelSizeTemporalMain, config.kernelSize, config.kernelSize),
                           doGlobalAveragePooling=config.doGlobalAveragePooling)
lossSmoothPred: Callable[[tf.Tensor, tf.Tensor], tf.Tensor]
lossSmoothPred = lossSmoothingPredictionCE

# %%
perFrameLoss = lambda t, p: (1 - config.weightCTCLoss) * lossPerFrame(t, p, config.weightOfBG) + \
                            config.weightSmoothing * lossSmoothPred(t, p)

if config.useSegmentationGuidedCTC:
    if config.usePrior:
        lossForCTC = lambda y, t: config.weightCTCLoss * lossCTCSimpleCustomRec_prior(y, t,
                                                                                      weightPrior=config.weightPrior)
        print("using segmentation guided CTC with prior of weight " + str(config.weightPrior))
        print("SSG" if config.doSSG else "HSG", "version")
    else:
        lossForCTC = lambda y, t: config.weightCTCLoss * lossCTCSimpleCustomRec(y, t)
        print("using segmentation guided CTC without prior")
        print("SSG" if config.doSSG else "HSG", "version")

else:
    if config.usePrior:
        lossForCTC = lambda t, p: config.weightCTCLoss * lossCTCLiuNormal(t, p, prior=True,
                                                                          weightPrior=config.weightPrior)
        print("using normal CTC with prior of weight " + str(config.weightPrior))
    else:
        if config.useCTCLiu:
            lossForCTC = lambda t, p: config.weightCTCLoss * lossCTCLiuNormal(t, p, prior=False,
                                                                              weightPrior=config.weightPrior)
            print("using normal CTC without prior, Liu implementation")
        else:
            lossForCTC = lambda t, p: config.weightCTCLoss * lossCTC_Simple_TF(t, p, prior=False)
            print("using normal CTC without prior, TF implementation")


# lossSmoothRej = lambda t,p :    lossSmoothingReject(t,p)


# binCrossEntropyLossDecoder = lambda t,p : lossDecoderBinary(t,p)


losses = [[lossForCTC], [perFrameLoss]]


# metricsMainOutput = [TAR_allValues_onlyClasses, FAR_allValues_onlyClasses, RejectRate_allValues_onlyClasses,
#            ClassifyBackgroundOnBackground, RejectRate_allValues, TAR_FAR_Diff,TAR_3FAR_Diff]
#



metricsAux = [TAR_Argmax_allValues_onlyClasses, FAR_Argmax_allValues_onlyClasses,
              RejectRate_Argmax_onlyClasses, TAR_FAR_Argmax_Diff, TAR_3FAR_Argmax_Diff,
              RejectRate_Argmax_allValues]
# metricsCTCCustom = []
metricsCTCCustom = []

opti = tf.keras.optimizers.Adam(learning_rate=0.001)




model.compile(opti,
              loss=losses,
              loss_weights=[1, 1],
              metrics=[metricsCTCCustom, metricsAux])
#%% md

### Prepare input data

#%%


def AugmentationRotation(input1, GT):
    """

    :param input1: input, dim [seq, dimensionsImage[0], dimensionsImage[1], canal(2)]
    :param GT: (input2,input3,recuMatrix)
    :return:
    """
    degree = tf.random.normal(shape=(),mean=0.0, stddev=10, dtype=tf.float32)
    rotation = degree * np.pi / 180.
    input1 = tfa.image.rotate(input1, rotation, interpolation="nearest")
    return input1,GT

def reformulateForCTCLoss(input1, GT):
    a, b,  recuMatrix = GT
    # a : Batch Time 1
    # recuMatrix : None (bach, Time (variable), U(variable), U (variable)
    # d None, U/2 (variable), 3
    mask = a  != -1

    shapeC = tf.shape(b)  # None, U/2, 3
    shapeMat = tf.shape(recuMatrix)  # None, Time, U, U
    shapes = tf.concat((shapeC, shapeMat), axis=-1)  # 7
    print("shapes", shapes)

    b = tf.reshape(b, [shapes[0] * shapes[1] * shapes[2]])
    recuMatrix = tf.reshape(recuMatrix, [shapes[3] * shapes[4] * shapes[5] * shapes[6]])

    # allTheMatrixIsNotLess1 = tf.reduce_all(tf.reduce_all(mask, axis=-1), axis=-1)
    # countOfValidTimeForEachBatch = tf.reduce_sum(tf.cast(allTheMatrixIsNotLess1,dtype=tf.int32), axis=-1)
    countOfRealTimeForEachBatch = tf.reduce_sum(tf.cast(tf.squeeze(mask, axis=-1), tf.int32), axis=-1)

    allInOneTensor = tf.concat((shapes, b, countOfRealTimeForEachBatch, recuMatrix), axis=0)
    print("allInOneTensor", allInOneTensor)
    return tf.cast(input1, tf.float32), (allInOneTensor, a)  # (a, b, c, allInOneTensor,input1)

def configureDataset(dataset:tf.data.Dataset,train=True,repeat=True):

    # dataset = dataset.take(nbTrain)
    # datasetValid = dataset.skip(nbTrain)
    if train:
        dataset = dataset.map(AugmentationRotation, num_parallel_calls=tf.data.AUTOTUNE)

        dataset = dataset.shuffle(buffer_size=nbTrain, reshuffle_each_iteration=True)

    toPad = (
        (tf.constant(0,dtype=tf.float32)),
        (tf.constant(-1,dtype=tf.int32), tf.constant(-1,dtype=tf.int32), tf.constant(-1,dtype=tf.int32))
    )
    # if is4D:
    #     input_shape = tf.TensorShape(
    #         [None, dimensionsImage[0], dimensionsImage[1], dimensionsImage[2],])
    # else:
    input_shape = tf.TensorShape([None, dimensionsImage[0], dimensionsImage[1],2])

    output_shapes = (input_shape,
                     (tf.TensorShape([None, 1]), tf.TensorShape([None, 3]), tf.TensorShape([None, None, None])))
    dataset = dataset.padded_batch(config.batchSize, padded_shapes=output_shapes,
                                           padding_values=toPad)

    dataset = dataset.map(reformulateForCTCLoss, num_parallel_calls=tf.data.AUTOTUNE)

    if repeat:
        dataset = dataset.repeat()
    return dataset



datasetTrain = tf.data.experimental.load(pathPreprocessedData)
datasetValid = tf.data.experimental.load(pathPreprocessedDataValid)


datasetTrain,datasetValid = configureDataset(datasetTrain),configureDataset(datasetValid,False)

#%% md

### Prepare callbacks for training

#%%


if not os.path.exists(pathLog):
    os.mkdir(pathLog)
os.mkdir(pathLog+date)
os.mkdir(pathWeight)
os.mkdir(pathLog+date+separator+"TensorBoard")
earlyStop = tf.keras.callbacks.EarlyStopping(monitor="val_loss", verbose=1, patience=40, mode='min')
checkpoint =  tf.keras.callbacks.ModelCheckpoint(pathWeight+separator+"model", monitor="val_loss", verbose=1, save_best_only=True,
                                     mode='min')
# tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=pathLog+ date+separator+"TensorBoard"+separator+"training", histogram_freq=1)
callbacks = [earlyStop,checkpoint,WandbCallback(save_model=False)]



#%% md

# Fitting

#%%

history = model.fit(datasetTrain, epochs=1000, steps_per_epoch=int(ceil(nbTrain // config.batchSize)), verbose=2,
                        validation_data=datasetValid, validation_steps= int(ceil(nbValid / config.batchSize)),
                        callbacks=callbacks)  # val_size / batchSize
print("fitted ! ", len(history.history['loss']), " epochs")

#%% md

### add some values in wandb

#%%

import tensorflow.keras.backend as K
trainable_count = np.sum([K.count_params(w) for w in model.trainable_weights])
wandb.log({"TrainableParams":trainable_count})
try:
    def myprint(s):
        print(s)
        with open(pathWeight+separator+"totalWeigth.txt", 'a+') as f:
            f.write(s+"\n")

    model.summary(print_fn=myprint)
except Exception as e:
    print("Problem with weight calculation 2")
    print(e)
copytree(pathLog+date + separator, wandBRunDir + separator + "weights")

# copyfile(ModelEarly2D.py,wandBRunDir+"ModelEarly2D.py")
wandb.finish()

#%%



