import os
from typing import List, Tuple

import numpy as np

from utils import wandbRecuperator
import tensorflow as tf


def strategyForCTC(predictionsOneHot)-> List[Tuple[int,int,int]]:
    #predictions : [seq,nbClass]
    assert len(predictionsOneHot) != 0
    predictions =  tf.argmax(predictionsOneHot,axis=1).numpy()
    predictions = predictions - 1.
    bounds = []
    currClass= predictions[0]
    startCurrentClass = 0
    for i in range(1, len(predictions)):
        pred = predictions[i]

        if pred != currClass or pred==-1 or i == len(predictions) - 1:# or (pred!= 0 and i>2 and windowTemporal[i]<windowTemporal[i-1]<=windowTemporal[i-2]<=windowTemporal[i-3]):
            if currClass!=-1:
                if i==len(predictions)-1 and pred==currClass:
                    endBound = i
                else:
                    endBound = i - 1
                bounds.append([int(currClass+1),startCurrentClass, endBound])  # should be i if i==len(listeSeq)-1 instead of i-1
            if i == len(predictions) - 1 and pred != currClass and pred != -1:
                bounds.append([int(pred+1),i, i])  # should be i if i==len(listeSeq)-1 instead of i-1
            currClass = pred
            startCurrentClass = i
    return bounds


def exportBoundsOneModel(modelName, pathModel,  PROJECTNAMEWANDB, pathPreprocessedDataTestWithoutAttribute,
                         attributeFoldSpecific=""):
    """
    example :
        pathDB = "C:\\workspace2\\Datasets\\Chalearn\\"
        separator = "\\"
        # attribute which is to the name of the folder containing the preprocessed data: "PreprocessedDataTest"+attribute
        pathPreprocessedDataTestWithoutAttribute = pathDB + "PreprocessedDataTest"
        pathCuDiSplitWithoutAttribute = pathDB + "CuDiSplit"
        pathProtocolFolder = pathDB + "Split" + separator
    :param pathModel:
    :param PROJECTNAMEWANDB:
    :param pathPreprocessedDataTestWithoutAttribute:
    :param pathCuDiSplitWithoutAttribute:
    :param pathProtocolFolder:
    :param attributeFoldSpecific:
    :return:
    """
    print("\t model", pathModel)

    if (not os.path.exists(pathModel)):
        os.makedirs(pathModel)
        wandbRecuperator.download_weights(modelName, pathModel, projectName=PROJECTNAMEWANDB)
    return
    # attribute = NamerFromHP.getNameFromHP(configParams)
    # attribute += "_"+attributeFoldSpecific
    # attribute = "R4D_10x10x10_split200_CuDi5_JointsNB13_customRec_Vox4_SSG_"#to remove
    attribute = attributeFoldSpecific
    print("\t\t attribute Prepro", attribute)
    pathPreprocessedDataTest = pathPreprocessedDataTestWithoutAttribute + attribute + "/"



    # def configTestDS(dataset):
    #     dataset = dataset.batch(1)
    #     return dataset

    try:
        datasetTest = tf.data.Dataset.load(pathPreprocessedDataTest).batch(1)
    except Exception as e:
        print("Error while loading the test dataset")
        print("possible cause : the dataset is not generated")
        print("Error :", e)
        exit(-1)

    # datasetTest = configTestDS(datasetTest)

    model = tf.keras.models.load_model(pathModel + "Weights/model", compile=False)
    opti = tf.keras.optimizers.Adam()
    model.compile(opti, loss=[], metrics=[])  # just to build

    iterator = iter(datasetTest)


    strategyAcceptation = strategyForCTC
    results: List[Tuple[str, np.ndarray, np.ndarray, np.ndarray]] = []
    # print("\t\t", len(testFiles), "files to test, start evaluating")
    for i, data in enumerate(iterator):
        # data : ( input             : (1, 217, 40, 40, 2)
        #        perFrameGT         : (1, 217, 1),
        #        class_start_end  : (1, 7, 3),
        # )
        nbGesture = tf.shape(data[2])[1]
        input = data[0]
        prediction = model(input, training=False)  # get prediction output [seq,nbClass]
        GT_start_end = data[2][0].numpy()  # List[GT,start,end] (ints)

        predictionCTC = prediction[1][0]
        # results.append((i, predictionCTC, GT_start_end))

        boundsPrediction = strategyAcceptation(predictionCTC)  # return class id, start, end
        # prediction = tf.argmax(prediction,axis=1).numpy()
        results.append((i, boundsPrediction, GT_start_end))
    if not os.path.exists(pathModel + "Bounds/"):
        os.makedirs(pathModel + "Bounds/" )
    if not os.path.exists(pathModel + "GT/"):
        os.makedirs(pathModel + "GT/")
    for file, bounds, GT_start_end in results:
        strToPrint = "\n".join([",".join(map(str, b)) for b in bounds])
        f = open(pathModel + "Bounds/" + str(file), "w+")
        f.write(strToPrint)
        f.close()
        # got a list of [class,start,end]
        # write it in a file, one line per gesture
        strToPrint = "\n".join([",".join(map(str, b)) for b in GT_start_end])
        # strToPrint = " ".join(map(lambda l: "\n".join(l), GT_start_end))
        f = open(pathModel + "GT/"  + str(file), "w+")
        f.write(strToPrint)
        f.close()

if __name__ == "__main__":
    def doOneTest():
    # Path of the DB
        pathDB = "C:\\workspace2\\Datasets\\2D\\ILGDB\\"
        separator = "\\"
        # attribute which is to the name of the folder containing the preprocessed data: "PreprocessedDataTest"+attribute
        pathPreprocessedDataTestWithoutAttribute = pathDB + "PreprocessedUntrimmed_WithCustomGraph\\Test\\"

        # Specify the model
        modelName = "glamorous-donkey-115"
        pathModel = pathDB + "Log" + separator + modelName + separator
        print("pathModel", pathModel)
        PROJECTNAMEWANDB = "precoce2d-CustomGraph"
        exportBoundsOneModel(modelName, pathModel,  PROJECTNAMEWANDB, pathPreprocessedDataTestWithoutAttribute,"")

def readFileAndExportBounds(db):
    PROJECTNAMEWANDB = "precoce2d-CustomGraph"
    pathDB = f"C:\\workspace2\\Datasets\\2D\\{db}\\"
    # pathDB = f"2D/{db}/"
    pathFileWithAllGroups = pathDB + "modelRunsGrouped/"
    pathOutputGeneral = pathDB + "expOut/"
    for file in os.listdir(pathFileWithAllGroups):
        f = open(pathFileWithAllGroups + file, "r")
        models = f.readlines()[0].split(",")
        f.close()

        modelNames = models
        nameFolderOut = file
        pathOutputG = pathOutputGeneral + nameFolderOut + "/"
        if not os.path.exists(pathOutputG):
            os.makedirs(pathOutputG)

        def doExportBounds(modelNames, pathOutput, nameOutputFolder):
            print("group", nameOutputFolder)
            for iFold, modelName in enumerate(modelNames):
                exportBoundsOneModel(modelName, pathOutput + modelName + "/", PROJECTNAMEWANDB,
                                     pathDB + "PreprocessedUntrimmed_WithCustomGraph/Test/",
                                     "")
                # break
        doExportBounds(modelNames=modelNames, pathOutput=pathOutputG, nameOutputFolder=nameFolderOut)
        # break
readFileAndExportBounds("ILGDB")