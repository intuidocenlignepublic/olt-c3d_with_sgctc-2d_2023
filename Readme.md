# Code for publication 2023 (in submission) 2D EXPERIMENTS
This code has been used to make the 2D experiments of the following paper:

"Early Gesture Detection in Untrimmed Streams: A Controlled CTC Approach for Reliable Decision-Making", William Mocaër; Eric Anquetil; Richard Kulpa. 2023
preprint : http://ssrn.com/abstract=4656381  

The code is written in python with tensorflow 2.10.

## Model
The package "Model" contains the model file ModelEarly2D.py
It's the OLT-C3D model (simple stream) 

## Losses and Metrics
It contains the losses and metrics used in the training, particularly the loss function (CTC) 
- The file "CustomReccurenceMatrix" allows to construct the graph paths of the CTC, 
it is used during the preprocessing, and stored with the examples in the Dataset
- the file LossesAndMetrics_CTC_CustomRec contains the high level loss function, 
it calls ctc_normal_customRec_tf which contains the low level custom function.


## FittedModel
The models are available on demand on the shadoc website soon: https://www-shadoc.irisa.fr/

## utils
Utils contains a parser for inkml partially developped by Robin Dahuron.

## Training.py
The file to use to train the model, it takes as input (args) the db name (ILGDB or MTGSetB) and the weight of the label prior ($\Psi$ in the paper)

## Databases
Each folder of database contains three files : 
- CreateAndProcessArtificalUntrimmed - create the artificals datasets by concatenating files (same way) and preprocess to generate the representations. Can export in original format using a boolean (doExport)
- VisualizingResult - export the results in image, as in qualitative results of the paper
- VisualizingResultWithConf - export the results in image, with temporal informations and confidence



The databases ILGDB and MTGSetB, Untrimmed versions, should be available on the intuidoc website: https://www-intuidoc.irisa.fr/en/mtgsetb-and-ilgdb-untrimmed/ 
if not you can contact Eric Anquetil or William Mocaër.

### myenv.yml
this file contains all dependencies necessary to run the code
The only known uncompability with older version of tf is the Dataset loading (tf.data.Dataset.load)
if needed, replace by tf.data.experimental.load
Run the training with libtcmalloc minimal ! (LD_PRELOAD=/usr/lib/libtcmalloc_minimal.so.4 python Training.py")
-> otherwise there will be memory problem during training due to data augmentation (known tf bug)


